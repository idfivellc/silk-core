export default class Hello {
    constructor(name: string) {
        console.log('Hello ' + name + '! You\'re all set!');
    }
}