# Silk Core (Beta)
Silk Core provide the skeleton for web projects providing minimal dependencies. The concept behind core is to add what you need nothing more or nothing less. Components can be pulled in from the silk library or you can add any library available via npm/yarn.

The overall goal of Silk 2.0 is to provide a flexible systems that allow developers hit the ground running without getting in there way.

## Design Principles
* Flexible
* Accessibility
* Easy to use
* Lightweight

## What comes in core
* Basic Scaffolding
* Normalize.css
* Modernizr
* Basic Helpers
* Basic Typography

## Technologies
* [webpack 2.0](https://webpack.js.org/)
* [PostCSS](http://postcss.org/)
	* [cssnext](http://cssnext.io/)
* [TypeScript](https://www.typescriptlang.org)
* [Yarn](https://yarnpkg.com/)

## Roadmap
### Silk Core
Finalize Silk Core. Address general workflow of how developers will use silk. How should modules should be added? Should we add the ability to

### Silk Components
Finalize List of components silk will provide.
*List of ideas*
* Acordions
* Tabs
* Modals
* CSS Animation Kit


@todo: Documentation