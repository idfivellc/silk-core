const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MainStylesheet =  new ExtractTextPlugin("assets/build/css/style.css");
const PrintStylesheet =  new ExtractTextPlugin("assets/build/css/print.css");

module.exports = {
    entry: "./assets/webpack.silk.ts",
    output: {
        filename: "assets/build/js/bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
            },
            {
                test: /style\.scss$/,
                use: MainStylesheet.extract({
                    fallback: "style-loader",
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /print\.scss$/,
                use: PrintStylesheet.extract({
                    fallback: "style-loader",
                    use: ['css-loader',  'sass-loader']
                })
            },
            {
                test: /\.(ttf|eot|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    name: '../../../assets/fonts/[name].[ext]',
                },
            },
        ]
    },
    resolve: {
        extensions: [".tsx", ".ts", ".js", ".scss"]
    },
    plugins: [
        MainStylesheet,
        PrintStylesheet
    ],
    devServer: {}
};
